package main;

import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class Client implements Runnable
{
	// Connection Variables
	private Socket clientSock;	
	ObjectInputStream fromClient; 
	ObjectOutputStream toClient;
	private Message msgFromClient;
	final Message waitMsg = new Message( Main.State.WAITING, 0, null, 0, 0 );

	// User specific variables
	private String name;
	
	// Game specific variables
	private int totalGuesses = 0;   // No of Guesses taken to win / lose	
	
	public String getName()
	{
		return this.name;
	}
	
	public int getTotalGuesses()
	{
		return this.totalGuesses;
	}
	
	public Client( Socket _clientSock ) throws IOException, ClassNotFoundException
	{		
		this.clientSock = _clientSock;
		toClient = new ObjectOutputStream(this.clientSock.getOutputStream());
		fromClient = new ObjectInputStream(this.clientSock.getInputStream());
		
		// Get client name
		inputName();
	}
	
	// Thread start method
	// DONT start this until ALL variables are set
	public void run()
	{
		try 
		{
			gameLoop();
			MultiplayerServer.logResults(name, totalGuesses);
			waitForResults();			
		} 
		catch (ClassNotFoundException | IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally 
		{
			try 
			{
				disconnect();
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Individual wait method, which also takes in the EXIT state from the server
	 */
	private void waitForResults() throws IOException, ClassNotFoundException
	{
		while( MultiplayerServer.gameActive == true )
		{
			// DO NOTHING
		}
		
		// Ensure client has shutdown by getting exit state from client
		msgFromClient = (Message) fromClient.readObject();
		
		if (Main.DEBUG)
			System.out.println( "WaitForResults-CLIENT state = " + Main.State.valueOf(
				msgFromClient.getState().toString() ) );
	}
	
	public void inputName() throws IOException, ClassNotFoundException
	{
		msgFromClient = (Message) fromClient.readObject();
		this.name = msgFromClient.getString();
		
		System.out.println( "CLIENT - ClientName = " + this.name);
	}
	
	public void gameLoop() throws IOException, ClassNotFoundException
	{
		Message clientMessage;
		int tempInt;	
		
		for (int guessNo = 1; guessNo <= Main.MAXGUESSES; guessNo++)
		{
			// Message client to make a guess 
			toClient.writeObject( new Message( Main.State.GUESS, guessNo, null, 0, 0 ) );
			
			// Receive a guess from client
			clientMessage = (Message) fromClient.readObject();
			
			if ( !(handleMsg( clientMessage, Main.State.GUESS)) )
				return;
				
			tempInt = clientMessage.getIntData();
			if (Main.DEBUG)
				System.out.println( "GuessInt = " + tempInt );
			
			System.out.print( name + " ");
			// Calculate clues
			if ( MultiplayerServer.gameGuesser.makeGuess( tempInt ) )
			{
				// SEND WIN MESSAGE TO CLIENT
				System.out.println(" ");
				System.out.println( name + " ### Correct guess after " + guessNo + " guesses ###");
				toClient.writeObject( new Message( Main.State.CORRECT, guessNo, 
						null, 0, 0) );
				totalGuesses = guessNo;
				break;
			}
			else if ( guessNo == 10 )  // SEND LOSS MESSAGE TO CLIENT
			{
				System.out.print( name + " INCORRECT - The correct no was ");
				MultiplayerServer.gameGuesser.printSecretNo(); 
				toClient.writeObject( new Message( Main.State.INCORRECT, 0, 
						MultiplayerServer.gameGuesser.getSecretNo(), 0, 0) );
				totalGuesses = guessNo;
			}
			else 	// Send clues to client
			{
				toClient.writeObject( new Message( Main.State.CLUES, 0, null, 
						MultiplayerServer.gameGuesser.getCorrectPos(), 
						MultiplayerServer.gameGuesser.getIncorrectPos() ));
				System.out.print( name + " NoOfGuesses = " + guessNo + " CorrectPositions = " + MultiplayerServer.gameGuesser.getCorrectPos() + "    ");
				System.out.println( "IncorrectPositions = " + MultiplayerServer.gameGuesser.getIncorrectPos() );
				clientMessage = (Message) fromClient.readObject();
				if ( !(handleMsg( clientMessage, Main.State.CLUES)) )
					return;
			}
		}
		// Receive a WIN / INCORRECT acknowledgement from client
		clientMessage = (Message) fromClient.readObject();
	}
	
	/**
	 * Check the message received from the client. If the client wishes to
	 * quit, that is handled here, which 
	 * 
	 * @param msg Message received from the client
	 * @param expectedState  State we are expecting to see
	 * @return
	 */
	private boolean handleMsg( Message msg, Main.State expectedState )
	{
		// If user wants to quit
		if ( msg.getState() == Main.State.QUIT )
		{
			System.out.println( "Client has elected to QUIT");
			return false;
		}
		else if ( msg.getState() != expectedState )
		{
			System.out.println( "ERROR - Incorrect MESSAGE received");
			return false;
		}
		return true;
	}
	
	public int userInputNoOfDigits() throws IOException, ClassNotFoundException
	{
		Message clientMessage;
		int tempInt;
		
		// Get noOfDigits from player 1
		toClient.writeObject( new Message( Main.State.INPUTDIGITS, 0, null, 0, 0 ) );
		
		// Receive noOfDigits
		clientMessage = (Message) fromClient.readObject();
		
//		if ( !(handleMsg( clientMessage, Main.State.INPUTDIGITS)) )
//			return 0;
			
		tempInt = clientMessage.getIntData();
		if (Main.DEBUG)
			System.out.println( "noOfDigits = " + tempInt );
		return tempInt;
	}
	
	public void advertiseNoOfDigits(int noOfDigits) throws IOException, ClassNotFoundException
	{
		toClient.writeObject( new Message( Main.State.GETDIGITS, noOfDigits, null, 0, 0 ) );
		// Receive response from client
		Message _clientMessage = (Message) fromClient.readObject();
	}
	
	public void disconnect() throws Exception
	{
		System.out.println( name + "CLIENT Disconnecting");
		this.fromClient.close();
		this.toClient.close();
		this.clientSock.close();
	}
}
