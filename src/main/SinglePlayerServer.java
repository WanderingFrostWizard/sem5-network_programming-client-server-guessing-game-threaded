package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.IllegalBlockingModeException;

public class SinglePlayerServer
{
	ServerSocket serverSock;
	Socket clientSock;
	
	ObjectOutputStream output;
	ObjectInputStream input;
	
	
	public SinglePlayerServer() throws IllegalBlockingModeException, SocketTimeoutException,
				SecurityException, IllegalArgumentException, UnknownHostException, IOException,
				ClassNotFoundException 
	{
		System.out.println("### SERVER ### ");
		NetworkTasks();
		
		try 
		{
			Game();
		}
		finally	// Regardless of how the game exits, we need to close the connections
		{
			CloseConnections();
		}
	}
	
	// ???? is this correct for print client address  
	//  System.out.println("Client Address: " + clientSock.getInetAddress().toString() );
	
	
	private void NetworkTasks() throws IllegalBlockingModeException, SocketTimeoutException,
				SecurityException, IllegalArgumentException, UnknownHostException, IOException 
	{
		// Open the Socket
		serverSock = new ServerSocket( Main.PRIPORTNO );

		System.out.println( "WAITING FOR CLIENT CONNECTION");
		// Listen for a client to connect
		clientSock = serverSock.accept();
		
		// Close the server socket to avoid any more clients connecting
//		serverSock.close();
		
		System.out.print( "CLIENT CONNECTED - ");
		// Print the clients socket address to console
		// ???? is this correct for print client address  
		System.out.println("Client Address: " + clientSock.getInetAddress().toString() );
		
		
		output = new ObjectOutputStream(clientSock.getOutputStream());
		input = new ObjectInputStream(clientSock.getInputStream());
	}
	
	private void CloseConnections() throws IOException
	{
		System.out.println( "SERVER SHUTTING DOWN");
		// Close the Socket
		input.close();
		output.close();
		clientSock.close();
		serverSock.close();
	}
	
	/**
	 * Function to run the game, by sending messages to the client.
	 * These messages send a STATE, and data to the client, to allow them
	 * to perform the correct sequence of actions to play the game, at the
	 * correct times
	 * 
	 * Stages   
	 *        inputDigits
	 *        GAME LOOP
	 *        	Guess
	 *        	Get clues
	 *          Correct Answer / No more guesses
	 *        Exit
	 *        
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void Game() throws IOException, ClassNotFoundException
	{
		Guesser gameGuesser;
		int tempInt = 0;
		Message clientMessage;
		
		// Message player 1 to select the number of digits
		output.writeObject( new Message( Main.State.INPUTDIGITS, 0, null, 0, 0 ) );
		
		// Receive noOfDigits from client
		clientMessage = (Message) input.readObject();
		
		if ( !(handleMsg( clientMessage, Main.State.INPUTDIGITS)) )
			return;
		
		tempInt = clientMessage.getIntData();
		System.out.println( "noOfDigits = " + tempInt );
		
		// Set NoOfDigits on our copy of Guesser to allow the player to use
		// functions such as inputGuessInt (with validation on the noOfDigits)
		gameGuesser = new Guesser( tempInt );
		
		// Generate a secret Number
		gameGuesser.initialize();
		
		// Start of Game Loop
		for (int guessNo = 1; guessNo <= Main.MAXGUESSES; guessNo++)
		{	
			System.out.println( "Guess no - " + guessNo + "  ");
			
			// Message client to make a guess 
			output.writeObject( new Message( Main.State.GUESS, guessNo, null, 0, 0 ) );
			
			// Receive a guess from client
			clientMessage = (Message) input.readObject();
			
			if ( !(handleMsg( clientMessage, Main.State.GUESS)) )
				return;
				
			tempInt = clientMessage.getIntData();
			if (Main.DEBUG)
				System.out.println( "GuessInt = " + tempInt );
			
			// Calculate clues
			if ( gameGuesser.makeGuess( tempInt ) )
			{
				// SEND WIN MESSAGE TO CLIENT
				System.out.println(" ");
				System.out.println( "### Correct guess after " + guessNo + " guesses ###");
				break;
			}
			else if ( guessNo == 10 )  // SEND LOSS MESSAGE TO CLIENT
			{
				System.out.print( "INCORRECT - The correct no was ");
				gameGuesser.printSecretNo();
			}
			else 	// Send clues to client
			{
				sendMsgToClient( new Message( Main.State.CLUES, 0, null, gameGuesser.getCorrectPos(), gameGuesser.getIncorrectPos() ));
//				output.writeObject( new Message( Main.State.CLUES, 0, null, gameGuesser.getCorrectPos(), gameGuesser.getIncorrectPos() ) );
				System.out.print( "CorrectPositions = " + gameGuesser.getCorrectPos() + "    ");
				System.out.println( "IncorrectPositions = " + gameGuesser.getIncorrectPos() );
				clientMessage = (Message) input.readObject();
				if ( !(handleMsg( clientMessage, Main.State.CLUES)) )
					return;
			}
		}

		if ( Main.DEBUG )
			System.out.println("Game Completed - Exiting");
		
		// Send exit message to client, telling them the game is complete
		output.writeObject( new Message( Main.State.EXIT, 0, gameGuesser.getSecretNo(), 0, 0 ) );
		
		// Wait for client to acknowledge this request, before shutting down 
		// the server
		clientMessage = (Message) input.readObject();
		
		if ( !(handleMsg( clientMessage, Main.State.EXIT)) )
		{
			System.out.println("ERROR client has yet to exit");
			return;
		}
	}
	
	/**
	 * Check the message received from the client. If the client wishes to
	 * quit, that is handled here, which 
	 * 
	 * @param msg Message received from the client
	 * @param expectedState  State we are expecting to see
	 * @return
	 */
	private boolean handleMsg( Message msg, Main.State expectedState )
	{
		// If user wants to quit
		if ( msg.getState() == Main.State.QUIT )
		{
			System.out.println( "Client has elected to QUIT");
			return false;
		}
		else if ( msg.getState() != expectedState )
		{
			System.out.println( "ERROR - Incorrect MESSAGE received");
			return false;
		}
		return true;
	}
	
	private void sendMsgToClient( Message msg ) throws IOException
	{
		if ( Main.DEBUG )
			System.out.println("WRITING OBJ");
		output.writeObject( msg );
		output.flush();
	}
}
