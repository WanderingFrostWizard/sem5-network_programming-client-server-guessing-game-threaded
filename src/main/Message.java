package main;

import java.io.Serializable;

/**
 * Generic message class, that is sent between server and clients
 * This is used as a template to ensure that each message is in the same format
 * When the receiver accepts the message, they can then interpret the data
 * 
 * @author Cameron Watt - s3589163
 */
public class Message implements Serializable
{
	private Main.State state;
	// Stores noOfDigits or guessNo 
	private int intData;
	// Stores Guesses or Secret No
	private int [] intArray;
	private int correctPos;
	private int incorrectPos;
	
	private String string;
	
	public Message( Main.State state, int intData, int [] intArray, int correctPos, int incorrectPos )
	{
		this.state = state;
		this.intData = intData;
		this.intArray = intArray;
		this.correctPos = correctPos;
		this.incorrectPos = incorrectPos;
	}
	
	public Message( String message )
	{
		this.string = message;
	}
	
	public String getString()
	{
		return this.string;
	}
	
	public Main.State getState()
	{
		return state;
	}
	
	public int getIntData()
	{
		return this.intData;
	}
	
	public int[] getintArray()
	{
		return this.intArray;
	}
	
	public int getCorrectPos()
	{
		return this.correctPos;
	}
	
	public int getIncorrectPos()
	{
		return this.incorrectPos;
	}
	
	
}
