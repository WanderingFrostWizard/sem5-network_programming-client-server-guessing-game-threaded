package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionManagerThread implements Runnable
{
    public void run()
    {
    	Socket clientSock;
    	ServerSocket serverSock;
    	
        try 
    	{
        	// Open the Socket
    		serverSock = new ServerSocket( Main.PRIPORTNO );
    		
		    while ( true )
		    {
		    	System.out.print("CON-MGR-> Clients Connected: " 
		    				+ MultiplayerServer.getNoOfClients());
		    	System.out.println("    ----------------------------------------");
		    	System.out.println( "CON-MGR-> WAITING FOR CLIENT CONNECTION");
				clientSock = serverSock.accept();
				
				System.out.print( "CON-MGR-> Client CONNECTED - ");
				// Print the clients socket address to console
				// ???? is this correct for print client address  
				System.out.println("Client Address: " + clientSock.getInetAddress().toString() );
				
				// Add client to the vector within the multiplayer Server class
				MultiplayerServer.newClient(clientSock);
		    }
//		    System.out.println( "Exiting Connection Manager");
    	}
        catch (IOException | ClassNotFoundException ex) 
    	{
			ex.printStackTrace();
		}
    }
}