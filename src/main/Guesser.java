package main;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class is included in all versions to give clients access to the static
 * variables and methods ONLY... THEY WILL NEED TO instantiate the class, but
 * THEY SHOULD NOT run the initialize() methods in this class, as it will 
 * generate another secret number, that will not be used and cause memory leaks
 * 
 * @author Cameron Watt
 */
public class Guesser  
{
	/*
	 * ########################################################################
	 *                         STATIC VARIABLES
	 * ########################################################################
	 */
	// NOTE due to the program design MAXDIGITS CANNOT exceed 9
	// As the only numbers that can be used are 1-9 hence 9 digits
	static final int MAXDIGITS = 8;
	static final int MINDIGITS = 3;
	
	/*
	 * ########################################################################
	 *                         CLASS VARIABLES
	 * ########################################################################
	 */
	// Number of digits in the secret number
	private int noOfDigits;
	
	// Highest and lowest possible numbers that can be produced with the  
	// current noOfDigits assuming no duplicate digits
	private int highestNo = 0;
	private int lowestNo = 0;
	
	//  Random Number generator
	private Random randomNoGen = new Random();
	
	// Array of digits that make the secret number
	private int [] secretNumber;
	private int [] guess;
	
	private int correctPos = 0;
	private int incorrectPos = 0;
	
	/*
	 * ########################################################################
	 *                           COMMON FUNCTIONS
	 * ########################################################################
	 */
	/**
	 * Constructor used by both client and server. Once this class has been 
	 * created and the noOfDigits set, other functions can be used
	 * 
	 * Other functions will cause UNPREDICTABLE BEHAVIOUR if noOfDigits is NOT 
	 * set correctly
	 * 
	 * @param _noOfDigits - The number of digits in the secret number of this 
	 * 				game, set by player one, and communicated to everyone else
	 */
	public Guesser( int _noOfDigits )
	{
		noOfDigits = _noOfDigits;
		// Calculate new highest and lowest possible numbers
		setLimits();
		guess = new int [ noOfDigits ];
	}
	
	public int getCorrectPos()
	{
		return this.correctPos;
	}
	
	public int getIncorrectPos()
	{
		return this.incorrectPos;
	}
	/*
	 * ########################################################################
	 *                   SERVER / GAMEMASTER SPECIFIC FUNCTIONS
	 * ########################################################################
	 */
	
	/**
	 * This function calls other functions required to generate the secret 
	 * number 
	 */
	protected void initialize()
	{
		generateSecretNo();
	}
	
	/**
	 * Function to calculate the highest and lowest possible numbers that can
	 * be produced with the current noOfDigits assuming no duplicate digits
	 */
	private void setLimits()
	{
		int power = (int) Math.pow( 10, ( MAXDIGITS - ( noOfDigits -1 ) ) );
		highestNo = 987654321 / power;
		lowestNo =  123456789 / power;
		if ( Main.DEBUG )
		{
			System.out.println( "HIGHEST No is " + highestNo );
			System.out.println( "LOWEST No is  " + lowestNo );
		}
	}

	/**
	 * Function to generate the secret number of noOfDigits in length, with NO
	 * duplicate digits and assign it to secretNumber array
	 */
	private void generateSecretNo( )
	{
		int newDigit = 0;
		int randIndex = 0;
		// Initialize secretNumber array, now that noOfDigits has been set
		secretNumber = new int [noOfDigits];
		
		// Create a list of the remaining numbers
		// Keeps track of what numbers are not contained within the secret number
		ArrayList<Integer> remDigits = new ArrayList<Integer>();
		for ( int i = 1; i < 10; i++ )
		{
			remDigits.add( new Integer(i) );
		}
		if ( Main.DEBUG )
			System.out.println( "ORIGINAL Values  = " + remDigits.toString() );
		
		// Select a random number from remDigits and add it to the secretNumber
		for ( int i = 0; i < noOfDigits; i++ )
		{
			if ( Main.DEBUG )
				System.out.println( "-----------------------------------------------");
			// Select a number at random
			randIndex = getRandInt( 0, remDigits.size() - 1 );
			newDigit = remDigits.get(randIndex).intValue();
			
			if ( Main.DEBUG )
				System.out.println("Removing " + newDigit );
			remDigits.remove(new Integer( newDigit ));
			secretNumber[i] = newDigit;
			
			if ( Main.DEBUG )
				System.out.println( "REMAINING Values = " + remDigits.toString() );
		}
		if ( Main.DEBUG )
		{
			printSecretNo();
		}	
	}

	/**
	 * Function to generate a random number between the specified ranges
	 * @param min - Minimum number that can be generated
	 * @param max - Maximum number that can be generated
	 * @return    - Number that is generated
	 */
	private int getRandInt( int min, int max )
	{
		return ( min + randomNoGen.nextInt( max + 1 - min ) );
	}
	
	/**
	 * Searches through the users guess, and the secretNumber to calculate
	 * the number of digits in the correct positions, and the number of digits
	 * that are correct, but in the wrong spots
	 * NOTE These values are stored as class variables, and accessed through 
	 * getters
	 * 
	 * @return true if the player has guessed the number correctly
	 */
	private boolean calculateClues()
	{
		// Reset the class variables
		// NOTE class variables used to allow access from other classes
		correctPos = 0;
		incorrectPos = 0;
		
		// Variable to avoid constant access to array
		int guessDigit = 0;
		
		// Search through arrays for Correct Positions
		for ( int i = 0; i < noOfDigits; i++ )
		{
			guessDigit = guess[i];
			// Correct Position
			if ( guessDigit == secretNumber[i] )
				correctPos++;
			// Incorrect Position
			else
			{
				// Check secretNumber Array for current guessDigit
				for ( int x = 0; x < noOfDigits; x++ )
				{
					if ( guessDigit == secretNumber[x] )
					{
						incorrectPos++;
						break;
					}						
				}
			}
		}		
		if ( correctPos == noOfDigits )
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Getter for the secret number - Needed to allow the server to get the
	 * number before sending it to each of the clients at the end of the round
	 * NOTE that this secret number is ONLY ever stored within the SERVER's 
	 * instance of the guesser
	 * 
	 * @return the secretNo array for this round
	 */
	protected int[] getSecretNo()
	{
		return this.secretNumber;
	}
	
	/*
	 * ########################################################################
	 *                   CLIENT SPECIFIC FUNCTIONS
	 * ########################################################################
	 */

	/**
	 * Ask the user for their guess as an integer, which can then be converted
	 * to an int array (guess format) using convertIntToGuess()
	 * 
	 * @return the valid guess
	 */
	protected int inputGuessInt(  )
	{
		int _guessInt = -1;
		String message = "Please enter a " + noOfDigits + " Digit number " +
				"in the form 12345678 WITHOUT duplicate digits or 0 to exit";

		while( true )
		{
			_guessInt = inputDigits( message );	
			
			// Check the value of the guess
			if ( _guessInt == 0 )
			{
				return 0;
			}
			// Check the guess is within the limits of possible numbers
			// Correct Input
			else if ( (_guessInt >= lowestNo ) && (_guessInt <= highestNo) )
				return _guessInt;	
		}	
	}
	
	/**
	 * Convert the guessInt into a guess Array, for comparison
	 * Returns true if the guess is CORRECT
	 * @param guessInt  Clients guess as an integer
	 */
	protected boolean makeGuess( int guessInt )
	{
		convertIntToGuess( guessInt );
		
		printGuess();
		
		return calculateClues();
	}
	
	public void convertIntToGuess( int guessInt)
	{
		// Break down the integer and fill the guess array in reverse
		for ( int i = noOfDigits - 1; i >= 0; i-- )
		{
			guess[i] = guessInt % 10;
			guessInt /= 10;
			if (Main.DEBUG)
				System.out.println("GuessInt = " + guessInt);
		}
	}
	
	public void printGuess( )
	{
		System.out.print("Clients Guess is ");
		printArray( guess );
	}
	
	public void printArray( int [] array )
	{
		for ( int i = 0; i < noOfDigits ; i++ )
		{
			System.out.print( array[i] + " : ");
		}
//		System.out.print("    ");
		System.out.println("");
	}
	
	protected void printSecretNo() 
	{
		System.out.print("Secret Number is ");
		printArray( secretNumber );
	}
	
	/*
	 * ########################################################################
	 *                      PLAYER 1 SPECIFIC FUNCTIONS
	 * ########################################################################
	 */
	
	/**
	 * Accessed from each of the game types to get the number of digits the 
	 * user would like in the secret number
	 * 
	 * This noOfDigits should be transmitted to ALL clients and the server
	 * who can then call the constructor  Guesser( int _noOfDigits ) to give
	 * them access to any necessary functions
	 * 
	 * @return noOfDigits entered by the user
	 */
	static int inputNoOfDigits()
	{
		int _guessInt = 0;
		String message = "Please enter the desired number of digits in the " +
				"Secret Number. Between " + Guesser.MINDIGITS + " and " + 
				Guesser.MAXDIGITS + " OR 0 TO QUIT";
		
		while (true)
		{
			_guessInt = inputDigits( message );

			// User wishes to quit
			if ( _guessInt == 0 )
			{
				return 0;
			}
			
			// Check the number of digits is within acceptable range
			if ( ( _guessInt >= Guesser.MINDIGITS ) && 
					( _guessInt <= Guesser.MAXDIGITS ) )
			{
				return _guessInt;
			}
		}
	}
	
	/**
	 * Generic function to take a message, which will be displayed to the user each
	 * time this function is called, and each time the user incorrectly enters
	 * data.
	 * @param msg  Instructional message to be displayed to the user
	 * @return   Value of the digits the user has entered. THIS value MUST be 
	 *      validated within the calling function
	 */
	static int inputDigits( String msg )
	{
		int _guessInt = 0;
		String input = null;
		
		while (true)
		{
			System.out.println( msg );
			input = Main.scanner.nextLine();
			
			try 
			{
				_guessInt = Integer.parseInt(input);
				
				// Check the number of digits is within acceptable range
				if ( _guessInt >= 0 )
				{
					return _guessInt;
				}
			}
			catch (NumberFormatException ex)
			{
				System.out.println( "ERROR NON Numerical characters detected");
			}
		}
	}
}