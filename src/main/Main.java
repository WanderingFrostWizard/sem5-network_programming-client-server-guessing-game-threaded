package main;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * The entry point for this program, regardless of what game mode you wish to
 * run, single, client-Server or multi-client-Server
 */
public class Main
{
	// ###### GLOBAL OBJECTS ######
	
	// Scanner object to get user input
	public static Scanner scanner = new Scanner( System.in );
	
	public static final boolean DEBUG = false;

//	public static final String HOST = "localhost";
	public static final String HOST = "m1-c33n1.csit.rmit.edu.au";		// null for loopback address
	public static final int PRIPORTNO = 19163;  
	public static final int SECPORTNO = 29163;
	
	static final int MAXGUESSES = 10;
	
	static SinglePlayerServer spServ;
	static PlayerClient client;
	static MultiplayerServer mpServ;
	static MultiPlayerClient mpClient;
	
	public enum State 
	{
	    WAITING, INPUTDIGITS, GETDIGITS, GUESS, CLUES, EXIT, QUIT, PLAYING, INCORRECT, CORRECT, RESULTS
	};
	
	public static void main(String[] args) 
	{
		try 
		{	
			// Local version on CONSOLE ONLY single thread
			if ( args.length == 0 )
				LocalSinglePlayer();
			
			// Single Client vs Server
			else if ( args[0].equals("server") )
				spServ = new SinglePlayerServer();
			else if ( args[0].equals("client") )
				client = new PlayerClient();
			
			// Multi Client vs Server
			else if ( args[0].equals("multiserver"))
				mpServ = new MultiplayerServer();
			else if ( args[0].equals("multiclient"))
				mpClient = new MultiPlayerClient();
			
			System.out.println( "THANKS FOR PLAYING ");
		}
	    catch (UnknownHostException ex) 
	    {
			error("UnknownHostException" , ex);
		} 
		catch (SocketTimeoutException ex )
		{
			error("Socket Connection timeout", ex);
		}
		catch (IOException ex) 
	    {
			error("IOException" , ex);
		}
		catch (SecurityException ex )
		{
			error("Security Issue with serverSock connection", ex);
		}
		catch( IllegalArgumentException ex )
		{
			error("Port out of range", ex);
		}
		catch (java.nio.channels.IllegalBlockingModeException ex)
		{
			error("No connection to accept for serverSock", ex);
		}
		catch (InterruptedException ex)
		{
			error("Exception in Wait methods", ex);
		}
		catch (ClassNotFoundException ex)
		{
			error("Class not found when passing Objects", ex);
		}
	}
	
	private static void LocalSinglePlayer()
	{
		int temp;
		
		System.out.println("### LOCAL ### ");
		
		temp = Guesser.inputNoOfDigits();
		if ( temp == 0 )
		{
			System.out.println( "Exiting Program" );
			return;
		}
		Guesser guesser = new Guesser( temp );
		
		// Generate a secret number
		guesser.initialize();
		
		// Start of Game Loop
		for (int guessNo = 1; guessNo <= MAXGUESSES; guessNo++)
		{
			System.out.print( "Guess no - " + guessNo + "  ");
					
			// Make guess
			temp = guesser.inputGuessInt();
			// Check if user wishes to quit
			if ( temp == 0 )
			{
				System.out.println( "Exiting Program" );
				return;
			}
			
			// Check the guess
			if ( guesser.makeGuess(temp))				
			{
				System.out.println( "### Correct guess after " + guessNo + " guesses ###");
				break;
			}
			else if ( guessNo == 10 )
			{
				System.out.print( "INCORRECT - The correct no was ");
				guesser.printSecretNo();
			}
			else
			{
				System.out.print( "CorrectPositions = " + guesser.getCorrectPos() + "    ");
				System.out.println( "IncorrectPositions = " + guesser.getIncorrectPos() );
			}
		} // End of Game Loop
	}

	/**
	 * Generic Error message function
	 * @param myText - Any message that I wish to add to help clarify
	 * @param ex - Exception encountered
	 */
	private static void error( String myText, Exception ex )
	{
		System.err.println( myText );
		System.err.println( ex.getMessage() );
		ex.printStackTrace();
	}
}
