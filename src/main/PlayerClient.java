package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.IllegalBlockingModeException;

public class PlayerClient 
{
	// Data Connection
	Socket serverSock = null;
	BufferedReader input = null;
	
	ObjectOutputStream outToServer;
	ObjectInputStream inFromServer;
	
	Message serverMessage = new Message( Main.State.WAITING, 0, null, 0, 0 );
	Message clientMessage;
	
	// ### Game specific variables ###
	Guesser clientGuesser;
	
	String playerName;
	int playerNo = 0;
	
	public PlayerClient() throws IllegalBlockingModeException, SocketTimeoutException,
		SecurityException, IllegalArgumentException, UnknownHostException, IOException,
		InterruptedException, ClassNotFoundException 
	{
		System.out.println("### PLAYER ### ");
		ClientNetworkTasks();
			
		try 
		{
			PlayGame();
		}
		finally	// Regardless of how the game exits, we need to close the connections
		{
			CloseConnections();
		}
	}
	
	private void ClientNetworkTasks() throws UnknownHostException, IOException
	{
		// Open Socket connection to server
		System.out.println("OPENING CONNECTION to: " + Main.HOST + " Port: " + Main.PRIPORTNO );
		serverSock = new Socket( Main.HOST, Main.PRIPORTNO);
		
		outToServer = new ObjectOutputStream(serverSock.getOutputStream());
		inFromServer = new ObjectInputStream(serverSock.getInputStream());
	}

	private void PlayGame() throws IOException, ClassNotFoundException
	{
		/*
		 *  Client game loop will exit, when game is successfully 
		 *  completed, or when user elects to QUIT
		 */
		do
		{
			serverMessage = (Message) inFromServer.readObject();
			
			if (Main.DEBUG)
				System.out.println( "Server state = " + Main.State.valueOf(
						 serverMessage.getState().toString() ) );
			
			// Follow instructions
			clientMessage = translateMessage( serverMessage );
			
			// Send response
			outToServer.writeObject(clientMessage);
		}
		while ( ( clientMessage.getState() != Main.State.EXIT ) &&
				( clientMessage.getState() != Main.State.QUIT ) );
	}
	
	/**
	 * This function translates a message from the server and performs the
	 * relevant actions, while preparing a response message to send back to the
	 * server
	 * 
	 * @param msg - Message received from the server
	 * @return New massage to return to the server
	 */
	private Message translateMessage( Message msg )
	{ 
		Main.State msgState = msg.getState();
		
		// New message variables
		Main.State tempState = Main.State.WAITING;
		int tempInt = 0;
		
		switch ( msgState )
		{
			// Server needs user to select the noOfDigits for the game
			// NOTE THIS IS ONLY DONE FOR PLAYER ONE
			// IF this client is NOT player one, they will receive this information
			// from the server
			case INPUTDIGITS:
			{
	//			if ( Main.DEBUG )
					System.out.println( "case INPUTDIGITS");
				
				tempState = Main.State.INPUTDIGITS;
				tempInt = Guesser.inputNoOfDigits();
				if ( tempInt == 0 )
				{
					// User wishes to EXIT Send QUIT state to server to exit
					// as well
					tempState = Main.State.QUIT;
				}
				
				// Initialize the clients guesser with the noOfDigits
				clientGuesser = new Guesser( tempInt );
				
				// SEND TO SERVER - DONE BELOW
				// SERVER TO SEND TO ALL CLIENTS
				break;
			}
			
			/* 
			 * If another player has chosen the noOfDigits, the Server will
			 * send this state to the other players, who can then use the 
			 * noOfDigits to initialize their own copy of the guesser object
			 * NOTE that this will ONLY be for access to the guesser functions
			 * NOT to generate their own secret number
			 */
			case GETDIGITS:
			{
	//			if ( Main.DEBUG )
					System.out.println( "case GETDIGITS");
				// Take the noOfDigits from the message and create a guesser
				// object with this information
				clientGuesser = new Guesser( msg.getIntData() );
				break;
			}
				
			case GUESS:
			{
				if ( Main.DEBUG )
					System.out.println( "case GUESS");
				
				// Print out the current guessNo to the client
				System.out.println( "Guess no - " + msg.getIntData() + "  ");
				
				// Return the guess as an int to server
				tempState = Main.State.GUESS;
				tempInt = clientGuesser.inputGuessInt();
				if ( tempInt == 0 )
				{
					// User wishes to EXIT Send QUIT state to server to exit
					// as well
					tempState = Main.State.QUIT;
				}
				
				clientGuesser.convertIntToGuess(tempInt);
				clientGuesser.printGuess();
				break;
			}			

			case CLUES:
			{
				if ( Main.DEBUG )
					System.out.println( "case CLUES");
					
				System.out.print( "CorrectPositions = " + msg.getCorrectPos() + "    ");
				System.out.println( "IncorrectPositions = " + msg.getIncorrectPos() );
				tempState = Main.State.CLUES;
				break;
			}
				
			/*
			 *  THIS flag is sent from the server when the game is completed
			 *  So its time to display the secret number
			 */
			case EXIT:
			{
				if ( Main.DEBUG )
					System.out.println( "case EXIT");
				System.out.println( "Secret Number was: ");
				clientGuesser.printArray( msg.getintArray() );
				// Acknowledge the servers request, and end the game
				tempState = Main.State.EXIT;
				break;
			}
			
			/*
			 * If the server shuts down pre-maturely, it sends this
			 * signal to the clients
			 */
			case QUIT:
			{
				//TODO ##############################################
				break;
			}
			
			case WAITING:
			{
				if ( Main.DEBUG )
					System.out.println( "case WAITING");
				break;
			}	
				
			default:
			{
				if ( Main.DEBUG )
					System.out.println( "case default");
				break;
			}
		}
		return new Message( tempState, tempInt, null, 0, 0 );
	}
	
	private void CloseConnections() throws IOException
	{
	    // CLOSE connections
	    System.out.println("CLOSING CLIENT CONNECTIONS" );
	    outToServer.close();
	    inFromServer.close();
		serverSock.close();
	}
}
