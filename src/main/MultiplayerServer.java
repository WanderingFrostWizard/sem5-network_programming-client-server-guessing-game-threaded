package main;

import java.net.Socket;
import java.util.Vector;
import java.io.IOException;

public class MultiplayerServer 
{
	final static int MAXPLAYERS = 3;
	final static int MAXCLIENTS = 6;
	
	private static int noOfClients = 0;
	private int noOfPlayers = 0;
	static Guesser gameGuesser;
	static boolean gameActive = false;
	static Vector<Client> connectedClients = new Vector<Client>();
	
	// Storage for the CURRENTLY playing players.
	// NOTE at the end of each game, this vector is cleared
	static Vector<Thread> players = new Vector<Thread>();
	
	// Storage for the results of THE CURRENT round
	static Vector<Results> results = new Vector<Results>();
	
	/**
	 * GAME SETUP LOOP
	 * Take clients Sockets from Vector
	 * Player 1 sets noOfDigits
	 * Server advertises the noOfDigits to remaining players
	 * Create Guesser / Generate Secret No
	 * 
	 * 	GAME PLAY LOOP
	 * 	Receive input(ANY PLAYER)
	 * 	Complete action based upon that players current status (from client CLASS)
	 * 	
	 * Calculate Winner (Lowest no of Guesses)
	 * Disconnect players
	 */
	public MultiplayerServer() throws IOException, ClassNotFoundException
	{
		// Start thread to handle client connections
		Thread conMgr = new Thread(new ConnectionManagerThread());
		conMgr.setDaemon(true);
		conMgr.start();
		
		// GAME SETUP LOOP
		// Is used to check if there is enough players then start the game
		while(true)
		{
			// Check number of clients connected
			noOfClients = connectedClients.size();
			
			// Set the current number of players
			if ( noOfClients >= MAXPLAYERS )
				noOfPlayers = MAXPLAYERS;
			else if ( noOfClients > 0 )
				noOfPlayers = noOfClients;
			
			// Start the game with the current number of players
			if ( noOfPlayers > 0 )
			{
				System.out.println("MULTI-SERVER - Starting Game");
				gameActive = true;
				
				// Send a PLAYGAME state to each of these clients
				sendMsgToALLPlayers( new Message( Main.State.PLAYING, 0, null, 0, 0 ) );
				
				playGame( );
				
				// Allow all clients to exit
								
				
				// Clear the results vector
				results.clear();
				
				// DISCONNECT players
				removePlayers();
				
				// Reset this variable as the game is now finished
				noOfPlayers = 0;
			}
//			System.out.println("End of current Game");
		}
	}
	
	/**
	 * Used to remove the players from the connectedClients list, as well
	 * as remove them from the list of threads
	 */
	private void removePlayers()
	{
		for( int i = 0; i < noOfPlayers; i++ )
		{
			// Removing clients from the HEAD of the list
			connectedClients.removeElementAt(0);
		}
		// All player threads can be removed, as their should only be
		// noOfPlayers * threads contained in it  
		// ie Only clients that are CURRENTLY playing are given a thread
		players.removeAllElements();  
	}
	
	/**
	 * Create a thread for each current player in the CURRENT game ONLY
	 * then add them to the players vector
	 */
	private void createThreadsForPlayers()
	{
		for( int i = 0; i < noOfPlayers; i++ )
		{
			players.add(new Thread( connectedClients.get(i)));
			players.get(i).start();
		}
	}
	
	/**
	 * This is the servers main game function. This function manages the 
	 * results of each players individual games, and collates them to 
	 * determine a winner
	 * NOTE this class stores the ONLY secret number generated in this game.
	 * It is THIS secret number that EACH player is trying to guess
	 */
	private void playGame() throws ClassNotFoundException, IOException
	{
		// Get noOfDigits from player 1
		int tempInt = connectedClients.get(0).userInputNoOfDigits();
		
		// Advertise the noOfDigits to remaining players
		for (int i = 1; i < noOfPlayers; i++)
		{
			connectedClients.get(i).advertiseNoOfDigits(tempInt);
		}
		
		// Set NoOfDigits on our copy of Guesser to allow the player to use
		// functions such as inputGuessInt (with validation on the noOfDigits)
		gameGuesser = new Guesser( tempInt );

		// Generate a secret Number
		gameGuesser.initialize();
		
		if ( Main.DEBUG)
			System.out.println("GAME PLAY LOOP");
		
		// GAME PLAY LOOP
		// Create threads for each player, and get these threads to run a 
		// Separate game loop for each player
		createThreadsForPlayers();
		
		// GAME RESULTS LOOP
		// ONLY ends after ALL players have finished their games
		while( results.size() < noOfPlayers )  // AT least one player must be active
		{
			// DO NOTHING		
		}
			
		// Calculate Winner
		int winner = calculateWinner();
		
		int [] resultsArray = resultsToArray();

		// Advertise Winner to ALL Players
		// All client objects are waiting for this to change
		// Once updated, the clients will know to get the final results
		gameActive = false;
		
//		TODO FOLLOWING MESSAGE is getting overridden with the quit state below it. To fix, 
//			each client will need some wait to deal with this...  To fix later
//		sendMsgToALLPlayers( new Message( Main.State.RESULTS, winner, resultsArray, 0, 0 ) );
		
		sendMsgToALLPlayers( new Message( Main.State.EXIT, winner, resultsArray, 0, 0 ) );
		
		System.out.println("#############################################################");
		System.out.println("SERVER - Winner is " + connectedClients.get(winner).getName());
		System.out.println("#############################################################");
	}
	
	private int [] resultsToArray()
	{
		int [] resultsArray = new int [MAXPLAYERS];
		
		for( int index = 0; index < MAXPLAYERS; index++ )
		{
			if ( index > noOfPlayers - 1 )
				resultsArray[index] = 0;
			else
				resultsArray[index] = connectedClients.get(index).getTotalGuesses();
		}
		
		return resultsArray;
	}
	
	/**
	 * Determine a winner, and return their index
	 * 
	 *    //TODO Fix bugs in the logic of this function
	 *    // Including if their is a draw of 2 or 3 players
	 *    // Preferably change this logic into a for loop
	 *    
	 * @return The index of the winning player
	 */
	private int calculateWinner( )
	{
		// If ONLY 1 PLAYER
		if ( noOfPlayers == 1 )
			return 0;
		// If TWO players
		else if ( noOfPlayers == 2 )
		{
		    if ( results.get(0).totalGuesses > results.get(1).totalGuesses )
		      	return 0;
		    else 
		    	return 1;
		}
		// If THREE players
		else if ( noOfPlayers == 3 )
		{
		    if ( results.get(0).totalGuesses > results.get(1).totalGuesses )
	      	{
		    	if ( results.get(0).totalGuesses > results.get(2).totalGuesses )
	    		{
		    		return 0;
	    		}
		    	else 
		    		return 2;
	      	}
		    else 
		    	if ( results.get(1).totalGuesses > results.get(2).totalGuesses )
	    		{
		    		return 1;
	    		}
		    	else 
		    		return 2;
		}
		
		System.out.println("UNKNOWN NUMBER OF PLAYERS - RESULTS CALCULATION");
		return 0;
	}

	private void sendMsgToALLPlayers( Message msg ) throws IOException 
	{
		for( int index = 0; index < noOfPlayers; index++ )
		{
			sendMsgToPlayer( index, msg );
		}
	}
	
	private void sendMsgToPlayer( int index, Message msg ) throws IOException 
	{
		connectedClients.get(index).toClient.writeObject(msg);
	}

	// ############################## STATIC FUNCTIONS ##############################
	
	/**
	 * Called by each Client object when they have finished their game
	 * 
	 * @param name - name of player
	 * @param totalGuesses - Total number of guesses taken by this player
	 */
	static void logResults( String name, int totalGuesses )
	{
		results.add(new Results( name, totalGuesses));
	}

	
	static int getNoOfClients()
	{
		return noOfClients;
	}
	
	// Called from the connectionManager thread
	static void newClient( Socket clientSock ) throws IOException, ClassNotFoundException 
	{
		connectedClients.addElement( new Client( clientSock ) );
	}
}
