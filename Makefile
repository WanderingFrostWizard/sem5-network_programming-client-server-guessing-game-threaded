

DIRECTORY = /home/s3589163/Moonshot
f0 = $(DIRECTORY)/src/main/Main.java
f1 = $(DIRECTORY)/src/main/Message.java
f2 = $(DIRECTORY)/src/main/Results.java
f3 = $(DIRECTORY)/src/main/Guesser.java
f4 = $(DIRECTORY)/src/main/SinglePlayerServer.java
f5 = $(DIRECTORY)/src/main/PlayerClient.java
f6 = $(DIRECTORY)/src/main/MultiPlayerClient.java
f7 = $(DIRECTORY)/src/main/MultiplayerServer.java
f8 = $(DIRECTORY)/src/main/ConnectionManagerThread.java
f9 = $(DIRECTORY)/src/main/Client.java

SOURCELIST = $(f0) $(f1) $(f2) $(f3) $(f4) $(f5) $(f6) $(f7) $(f8) $(f9) 

#javac -d bin -sourcepath $(SOURCELIST)
ALL:
	javac -d $(DIRECTORY)/bin $(SOURCELIST)

clean:
		$(RM) *.class